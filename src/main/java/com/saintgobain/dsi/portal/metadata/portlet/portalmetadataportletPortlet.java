package com.saintgobain.dsi.portal.metadata.portlet;

import com.saintgobain.dsi.portal.metadata.constants.portalmetadataportletPortletKeys;

import java.io.IOException;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author liferay
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.training",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=portal-metadata-portlet Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + portalmetadataportletPortletKeys.portalmetadataportlet,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class portalmetadataportletPortlet extends MVCPortlet {
	
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		
		String viewType = renderRequest.getParameter("viewType");    
		if(Validator.isNotNull(viewType) && viewType.equals("User")){
			PortletRequestDispatcher dispatcheraddCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/user.jsp");
			dispatcheraddCourse.include(renderRequest,renderResponse);
			
		}else if(Validator.isNotNull(viewType) && viewType.equals("Site")){
			PortletRequestDispatcher dispatcheraddCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/site.jsp");
			dispatcheraddCourse.include(renderRequest,renderResponse);
		}
		else if(Validator.isNotNull(viewType) && viewType.equals("UserAccessSite")){
			PortletRequestDispatcher dispatcheraddCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/userAccessSite.jsp");
			dispatcheraddCourse.include(renderRequest,renderResponse);
		}
		else if(Validator.isNotNull(viewType) && viewType.equals("Home")){
			PortletRequestDispatcher dispatcheraddCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/view.jsp");
			dispatcheraddCourse.include(renderRequest,renderResponse);
		}
		else{
			PortletRequestDispatcher dispatcheraddCourse = getPortletContext().getRequestDispatcher("/META-INF/resources/view.jsp");
			dispatcheraddCourse.include(renderRequest,renderResponse);
		}
	
	}
	
}
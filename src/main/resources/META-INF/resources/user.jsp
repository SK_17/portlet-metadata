<%@page import="com.liferay.portal.kernel.service.UserLocalServiceUtil"%>
<%@ page import="java.util.List" %>
<%@page import="com.liferay.portal.kernel.model.User"%>
<%@ page import="com.liferay.portal.kernel.exception.PortalException" %>

<h1>User List</h1>
<%

List<User> userList = UserLocalServiceUtil.getUsers(-1, -1);
{
%>

<table class="table table-light">
            
            <tr>
            <th scope="col">User Id</th>
            <th scope="col">User Name</th>
            <th scope="col">User Email</th>
            </tr>
            
  <% for(User user:userList){%>        
      
            <tr>
            <th scope="col"><%= user.getUserId()%></th>
            <th scope="col"><%= user.getFullName()%></th>
            <th scope="col"><%= user.getEmailAddress()%></th>
            
           </tr>
  <%} %>
</table>
<%
}
%>
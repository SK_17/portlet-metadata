<%@page import="com.liferay.portal.kernel.model.User"%>
<%@page import="com.liferay.portal.kernel.theme.ThemeDisplay"%>
<%@ include file="/init.jsp" %>
<% 

if(themeDisplay.isSignedIn()){
	

%>

 
<a class="btn btn-sm  btn-danger"  href="<%=listUserViewURL%>">User</a>
<a class="btn btn-sm  btn-danger"  href="<%=listSiteViewURL%>">Site</a>
<a class="btn btn-sm  btn-danger"  href="<%=listUserSiteViewURL%>">User Access Pages</a>

<%
}
else{
	
	out.print("Please login to access this portlet");
}

%>
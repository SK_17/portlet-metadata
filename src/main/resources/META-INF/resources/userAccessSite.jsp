<%@page import="com.liferay.portal.kernel.theme.ThemeDisplay"%>

<%@ include file="/init.jsp" %>
<h1>User Managing Page</h1>
<%@page import="com.liferay.portal.kernel.model.Layout"%>

<%
List<Layout> layoutlist = LayoutLocalServiceUtil.getLayouts(themeDisplay.getScopeGroupId(), false);
%>

<table class="table table-light">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Friendly URL</th>
      <th scope="col">User</th>
    </tr>
  </thead>
  <tbody>
    <% for(Layout currentLayout : layoutlist) { %>
    <tr>
      <td><%= currentLayout.getName(themeDisplay.getSiteGroupName())%></td>
      <td><%= currentLayout.getFriendlyURL()%></td>
      <td><%= currentLayout.getUserName()%></td>
      
    </tr>
    <% } %>
  </tbody>
</table>

<%@page import="com.liferay.portal.kernel.service.GroupLocalServiceUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.List" %>
<%@ include file="/init.jsp" %>

<%@page import="com.liferay.portal.kernel.model.Group"%>
<h1>Site List</h1>
<a class="btn btn-sm  btn-danger"  href="<%=homeViewURL%>">Home</a>
<%


List<Group> siteList = GroupLocalServiceUtil.getGroups(-1, -1);
{
%>

<table class="table table-light">
            
            <tr>
            <th scope="col"> Id</th>
            <th scope="col"> Name</th>
            
            </tr>
            
  <% for(Group site:siteList){
        if(site.isSite()){%>        
      
            <tr>
            <th scope="col"><%= site.getGroupId()%></th>
            <th scope="col"><%= site.getName()%></th>
             
          
           </tr>
  <%}
        }%>
</table>
<%
}
%>